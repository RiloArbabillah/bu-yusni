<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kelas_id')->nullable();
            $table->string('alternatif')->nullable();
            $table->string('nama');
            $table->string('sikap')->nullable();
            $table->integer('sikap_bobot')->nullable();
            $table->float('pengetahuan', 4, 2)->nullable();
            $table->integer('pengetahuan_bobot')->nullable();
            $table->float('keterampilan', 4, 2)->nullable();
            $table->integer('keterampilan_bobot')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}
