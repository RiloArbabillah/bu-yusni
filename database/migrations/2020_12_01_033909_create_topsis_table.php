<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topsis', function (Blueprint $table) {
            $table->id();
            $table->integer('mahasiswa_id');
            $table->integer('c1')->nullable();
            $table->integer('c2')->nullable();
            $table->integer('c3')->nullable();
            $table->integer('e1')->nullable();
            $table->integer('e2')->nullable();
            $table->integer('e3')->nullable();
            $table->float('r1')->nullable();
            $table->float('r2')->nullable();
            $table->float('r3')->nullable();
            $table->float('y1')->nullable();
            $table->float('y2')->nullable();
            $table->float('y3')->nullable();
            $table->float('x1')->nullable();
            $table->float('x2')->nullable();
            $table->float('x3')->nullable();
            $table->float('d1')->nullable(); // d+
            $table->float('d2')->nullable(); // d-
            $table->float('d3')->nullable(); // preferensi
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topsis');
    }
}
