<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAhpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahp', function (Blueprint $table) {
            $table->id();
            $table->string('jenis')->nullable(); // (Perbandingan, normalisasi, eigen, perkalian) berpasangan
            $table->string('ket')->nullable(); // C1,C2,C3
            $table->float('n1', 6, 3)->nullable();
            $table->float('n2', 6, 3)->nullable();
            $table->float('n3', 6, 3)->nullable();
            $table->float('n_total', 6, 3)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahp');
    }
}
