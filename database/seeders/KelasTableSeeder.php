<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class KelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Kelas::truncate();
        $list = range(ord('A'), ord('E'));
        foreach ($list as $kode) {
            \App\Models\Kelas::create([
                'nama' => 'Kelas '. chr($kode),
                'kode' => chr($kode),
            ]);
        }
    }
}
