<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AhpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Ahp::truncate();

        $kriteria = [
            ['ket' => 'C1', 'n1' => '1.00', 'n2' => '5.00', 'n3' => '0.33'],
            ['ket' => 'C2', 'n1' => '0.20', 'n2' => '1.00', 'n3' => '1.67'],
            ['ket' => 'C3', 'n1' => '0.20', 'n2' => '0.50', 'n3' => '1.00'],
        ];
        $pembagi = [
            'n1' => '1.40',
            'n2' => '6.50',
            'n3' => '3.00',
        ];
        // (Perbandingan, normalisasi, eigen, perkalian) berpasangan

        foreach ($kriteria as $index => $item) {
            \App\Models\Ahp::create([
                'jenis' => 'perbandingan',
                'ket' => $item['ket'],
                'n1' => $item['n1'],
                'n2' => $item['n2'],
                'n3' => $item['n3'],
            ]);
        }

        foreach ($kriteria as $index => $item) {
            \App\Models\Ahp::create([
                'jenis' => 'normalisasi',
                'ket' => $item['ket'],
                'n1' => $item['n1']/$pembagi['n1'],
                'n2' => $item['n2']/$pembagi['n2'],
                'n3' => $item['n3']/$pembagi['n3'],
            ]);
        }

        foreach ($kriteria as $index => $item) {
            $ket = array_keys($item)[$index+1];
            \App\Models\Ahp::create([
                'jenis' => 'eigen',
                'ket' => $item['ket'],
                'n1' => ($item['n1']/$pembagi['n1'] + $item['n2']/$pembagi['n2'] + $item['n3']/$pembagi['n3'])/3,
            ]);
        }

        $eigen = \App\Models\Ahp::where('jenis', 'eigen')->get();
        $perbandingan = \App\Models\Ahp::where('jenis', 'perbandingan')->get();
        foreach ($perbandingan as $index => $item) {
            $hasil = $item->n1*$eigen[0]->n1+$item->n2*$eigen[1]->n1+$item->n3*$eigen[2]->n1;
            \App\Models\Ahp::create([
                'jenis' => 'perkalian',
                'ket' => $item->ket,
                'n1' => $hasil,
            ]);
        }
    }
}
