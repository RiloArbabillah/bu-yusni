<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mahasiswa;

class Topsis extends Model
{
    use HasFactory;

    protected $table = 'topsis';

    protected $guarded = ['
        id, created_at, updated_at
    '];

    public function mahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, 'id', 'mahasiswa_id');
    }
}
