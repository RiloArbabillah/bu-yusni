<?php

namespace App\Http\Controllers;

use App\Models\Topsis;
use App\Models\Ahp;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class TopsisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::all();
        $eucli = Mahasiswa::selectRaw("
                'Jarak Euclidien' judul,
                sum(sikap_bobot) ei1,
                sum(pengetahuan_bobot) ei2,
                sum(keterampilan_bobot) ei3
            ")
            ->first();

        Topsis::truncate();
        // create the c, eucli, normalisasi
        // number_format((float)$item->sikap_bobot/sqrt($eucli[0]->ei1), 4, '.', '')
        foreach ($mahasiswa as $index => $item)
        {
            Topsis::create([
                'mahasiswa_id' => $item->id,
                'c1' => $item->sikap_bobot,
                'c2' => $item->pengetahuan_bobot,
                'c3' => $item->keterampilan_bobot,
                'e1' => $eucli->ei1,
                'e2' => $eucli->ei2,
                'e3' => $eucli->ei3,
                'r1' => $item->sikap_bobot/$eucli->ei1,
                'r2' => $item->pengetahuan_bobot/$eucli->ei2,
                'r3' => $item->keterampilan_bobot/$eucli->ei3,
            ]);
        }
        $total_max = Topsis::selectRaw('
            max(r1) r1,
            max(r2) r2,
            max(r3) r3
        ')->first();
        $total_min = Topsis::selectRaw('
            min(r1) r1,
            min(r2) r2,
            min(r3) r3
        ')->first();

        foreach ($mahasiswa as $index => $item) {
            Topsis::where('mahasiswa_id', $item->id)->update([
                'y1' => $total_max->r1,
                'y2' => $total_max->r2,
                'y3' => $total_max->r3,
                'x1' => $total_min->r1,
                'x2' => $total_min->r2,
                'x3' => $total_min->r3,
                'd1' => sqrt(pow($item->sikap_bobot/$eucli->ei1 - $total_max->r1, 2)
                        + pow($item->pengetahuan_bobot/$eucli->ei2 - $total_max->r2, 2)
                        + pow($item->keterampilan_bobot/$eucli->ei3 - $total_max->r3, 2)),
                'd2' => sqrt(pow($item->sikap_bobot/$eucli->ei1 - $total_min->r1, 2)
                        + pow($item->pengetahuan_bobot/$eucli->ei2 - $total_min->r2, 2)
                        + pow($item->keterampilan_bobot/$eucli->ei3 - $total_min->r3, 2)),
                'd3' => sqrt(
                        pow($item->sikap_bobot/$eucli->ei1 - $total_min->r1, 2)
                        + pow($item->pengetahuan_bobot/$eucli->ei2 - $total_min->r2, 2)
                        + pow($item->keterampilan_bobot/$eucli->ei3 - $total_min->r3, 2))
                        / (
                            sqrt(pow($item->sikap_bobot/$eucli->ei1 - $total_max->r1, 2)
                            + pow($item->pengetahuan_bobot/$eucli->ei2 - $total_max->r2, 2)
                            + pow($item->keterampilan_bobot/$eucli->ei3 - $total_max->r3, 2))
                            +
                            sqrt(pow($item->sikap_bobot/$eucli->ei1 - $total_min->r1, 2)
                            + pow($item->pengetahuan_bobot/$eucli->ei2 - $total_min->r2, 2)
                            + pow($item->keterampilan_bobot/$eucli->ei3 - $total_min->r3, 2))
                        )
            ]);
        }

        // $topsis = Topsis::with('mahasiswa')->get();
        $topsis = Topsis::with('mahasiswa')->get()
        ->transform(function($data){
            return [
                'id' => $data->id,
                'nama' => $data->mahasiswa->nama,
                'alternatif' => $data->mahasiswa->alternatif,
                'c1' => $data->c1,
                'c2' => $data->c2,
                'c3' => $data->c3,
                'e1' => $data->e1,
                'e2' => $data->e2,
                'e3' => $data->e3,
                'r1' => $data->r1,
                'r2' => $data->r2,
                'r3' => $data->r3,
                'y1' => $data->y1,
                'y2' => $data->y2,
                'y3' => $data->y3,
                'x1' => $data->x1,
                'x2' => $data->x2,
                'x3' => $data->x3,
                'd1' => $data->d1,
                'd2' => $data->d2,
                'd3' => $data->d3,
            ];
        });

        return view('topsis.index', compact('topsis', 'eucli', 'total_max', 'total_min'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topsis  $topsis
     * @return \Illuminate\Http\Response
     */
    public function show(Topsis $topsis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topsis  $topsis
     * @return \Illuminate\Http\Response
     */
    public function edit(Topsis $topsis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topsis  $topsis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topsis $topsis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topsis  $topsis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topsis $topsis)
    {
        //
    }
}
