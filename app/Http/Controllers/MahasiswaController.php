<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\Kelas;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        $list_mahasiswa = Mahasiswa::with('kelas')->get();
        return view('mahasiswa.index', compact('list_mahasiswa'));
    }

    public function create()
    {
        $list_sikap = [
            ['id' => 1, 'nama' => 'Buruk'],
            ['id' => 2, 'nama' => 'Kurang Baik'],
            ['id' => 3, 'nama' => 'Cukup'],
            ['id' => 4, 'nama' => 'Baik'],
            ['id' => 5, 'nama' => 'Sangat Baik'],
        ];

        return view('mahasiswa.form', compact('list_sikap'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'max:100'],
            'sikap' => ['required', 'numeric'],
            'pengetahuan' => ['required', 'numeric'],
            'keterampilan' => ['required', 'numeric'],
        ]);

        $list_sikap = [
            ['id' => 1, 'nama' => 'Buruk'],
            ['id' => 2, 'nama' => 'Kurang Baik'],
            ['id' => 3, 'nama' => 'Cukup'],
            ['id' => 4, 'nama' => 'Baik'],
            ['id' => 5, 'nama' => 'Sangat Baik'],
        ];

        $count = Mahasiswa::count()+1;
        $alternatif = 'A'.$count;

        $mahasiswa = Mahasiswa::firstOrCreate([
            'nama' => $request->nama,
            'alternatif' => $alternatif
        ]);

        if ($request->pengetahuan >= 95) {
            $pengetahuan = 5;
        }elseif ($request->pengetahuan >= 90) {
            $pengetahuan = 4;
        }elseif ($request->pengetahuan >= 85) {
            $pengetahuan = 3;
        }elseif ($request->pengetahuan >= 80) {
            $pengetahuan = 2;
        }else {
            $pengetahuan = 1;
        }

        if ($request->keterampilan >= 95) {
            $keterampilan = 5;
        }elseif ($request->keterampilan >= 90) {
            $keterampilan = 4;
        }elseif ($request->keterampilan >= 85) {
            $keterampilan = 3;
        }elseif ($request->keterampilan >= 80) {
            $keterampilan = 2;
        }else {
            $keterampilan = 1;
        }

        if ($mahasiswa->wasRecentlyCreated) {
            $mahasiswa->update([
                'sikap' => $list_sikap[$request->sikap-1]['nama'],
                'sikap_bobot' => $request->sikap,
                'pengetahuan' => $request->pengetahuan,
                'pengetahuan_bobot' => $pengetahuan,
                'keterampilan' => $request->keterampilan,
                'keterampilan_bobot' => $keterampilan,
            ]);
            return redirect()->route('mahasiswa.index')->withSuccess('Mahasiswa '.$request->nama.' berhasil ditambahkan');
        }else{
            return redirect()->back()->withErrors('Mahasiswa '.$request->nama.' sudah ada');
        }
    }

    public function edit($id)
    {
        $mahasiswa = Mahasiswa::find($id);

        $list_sikap = [
            ['id' => 1, 'nama' => 'Buruk'],
            ['id' => 2, 'nama' => 'Kurang Baik'],
            ['id' => 3, 'nama' => 'Cukup'],
            ['id' => 4, 'nama' => 'Baik'],
            ['id' => 5, 'nama' => 'Sangat Baik'],
        ];

        return view('mahasiswa.form', compact('list_sikap', 'mahasiswa'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => ['required', 'max:100'],
            'pengetahuan' => ['required', 'numeric'],
            'keterampilan' => ['required', 'numeric'],
        ]);

        $list_sikap = [
            ['id' => 1, 'nama' => 'Buruk'],
            ['id' => 2, 'nama' => 'Kurang Baik'],
            ['id' => 3, 'nama' => 'Cukup'],
            ['id' => 4, 'nama' => 'Baik'],
            ['id' => 5, 'nama' => 'Sangat Baik'],
        ];

        if ($request->pengetahuan >= 95) {
            $pengetahuan = 5;
        }elseif ($request->pengetahuan >= 90) {
            $pengetahuan = 4;
        }elseif ($request->pengetahuan >= 85) {
            $pengetahuan = 3;
        }elseif ($request->pengetahuan >= 80) {
            $pengetahuan = 2;
        }else {
            $pengetahuan = 1;
        }

        if ($request->keterampilan >= 95) {
            $keterampilan = 5;
        }elseif ($request->keterampilan >= 90) {
            $keterampilan = 4;
        }elseif ($request->keterampilan >= 85) {
            $keterampilan = 3;
        }elseif ($request->keterampilan >= 80) {
            $keterampilan = 2;
        }else {
            $keterampilan = 1;
        }

        $mahasiswa = Mahasiswa::find($id)->update([
            'nama' => $request->nama,
            'pengetahuan' => $request->pengetahuan,
            'pengetahuan_bobot' => $pengetahuan,
            'keterampilan' => $request->keterampilan,
            'keterampilan_bobot' => $keterampilan,
        ]);

        if ($request->sikap) {
            $mahasiswa = Mahasiswa::find($id)->update([
                'sikap' => $list_sikap[$request->sikap-1]['nama'],
                'sikap_bobot' => $request->sikap,
            ]);
        }

        if ($mahasiswa) {
            return redirect()->route('mahasiswa.index')->withSuccess('Mahasiswa '.$request->nama.' berhasil disimpan');
        }else{
            return redirect()->back()->withErrors('Mahasiswa '.$request->nama.' gagal disimpan');
        }
    }

    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        if ($mahasiswa) {
            $mahasiswa->delete();
            return redirect()->back()->withSuccess('Mahasiswa '.$mahasiswa->nama.' berhasil dihapus');
        }else{
            return redirect()->back()->withErrors('Mahasiswa '.$mahasiswa->nama.' tidak ditemukan');
        }
    }
}
