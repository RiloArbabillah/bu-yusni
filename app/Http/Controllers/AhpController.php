<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Models\Ahp;

class AhpController extends Controller
{
    public function index()
    {
        $pembagi = [
            'n1' => '1.40',
            'n2' => '6.50',
            'n3' => '3.00',
        ];

        // (Perbandingan, normalisasi, eigen, perkalian) berpasangan
        $perbandingan = Ahp::where('jenis', 'perbandingan')->get();
        $normalisasi = Ahp::where('jenis', 'normalisasi')->get();
        $eigen = Ahp::where('jenis', 'eigen')->get();
        $perkalian = Ahp::where('jenis', 'perkalian')->get();

        $mahasiswa = Mahasiswa::all();
        return view('ahp.index', compact('perbandingan', 'normalisasi', 'eigen', 'perkalian', 'pembagi'));
    }

    public function create()
    {
        dd('create');
    }

    public function store(Request $request)
    {
        dd('store');
    }

    public function edit($id)
    {
        dd('edit');
    }

    public function update($id, Request $request)
    {
        dd('update');
    }

    public function destroy($id)
    {
        dd('destroy');
    }
}
