<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<title>
		Project {{ env('APP_NAME') }}
	</title>
</head>
<body class="bg-gray-200">
  {{-- Header Link --}}
	<div class="h-10-vh bg-indigo-600 flex items-center justify-center text-gray-100 font-semibold space-x-5 px-5 md:px-10 lg:px-20">
    <a href="/"
      class="px-4 py-2 rounded-md {{ Request::is('/')? 'bg-indigo-500' : 'hover:bg-indigo-500' }}"
    >
      Dashboard
    </a>
    <a href="{{ route('mahasiswa.index') }}"
      class="px-4 py-2 rounded-md {{ Request::is('mahasiswa/*')? 'bg-indigo-500' : 'hover:bg-indigo-500' }}"
    >
      Mahasiswa
    </a>
    <a href="{{ route('ahp.index') }}"
      class="px-4 py-2 rounded-md {{ Request::is('ahp/*')? 'bg-indigo-500' : 'hover:bg-indigo-500' }}"
    >
      Metode AHP
    </a>
    <a href="{{ route('topsis.index') }}"
      class="px-4 py-2 rounded-md {{ Request::is('topsis/*')? 'bg-indigo-500' : 'hover:bg-indigo-500' }}"
    >
      Metode TOPSIS
    </a>
  </div>
  {{-- Yield Body --}}
	<div class="h-80-vh overflow-hidden overflow-y-auto px-5 md:px-10 lg:px-20">
    <div class="w-full justify-center flex">
      @if($errors->any())
        <div class="w-full lg:-mx-20 md:-mx-10 -mx-5 bg-red-500">
          @foreach ($errors->all(':message') as $item)
            <div class="text-white text-center px-10 py-2">
              {{ $item }}
            </div>
          @endforeach
        </div>
      @elseif(session()->has('success'))
        <div class="w-full lg:-mx-20 md:-mx-10 -mx-5 bg-green-500">
          <div class="text-white text-center px-10 py-2">
            {{ session()->get('success') }}
          </div>
        </div>
      @endif
    </div>
    <div class="w-full justify-center flex">
      @yield('content')
    </div>

    {{-- Yield Footer --}}
	</div>
  <div class="w-full px-5 md:px-10 lg:px-20 py-5 flex flex-col">
    <span>
      &copy; 2020 Pengembang Aplikasi
    </span>
    <span>
      {{ $creator }}
    </span>
    {{-- <span>
      {{ $note }}
    </span> --}}
  </div>
</body>
</html>
