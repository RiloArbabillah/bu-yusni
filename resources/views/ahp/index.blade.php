@extends('_layouts.default')
@section('content')
<div>
  <div class="py-2">
    <h1 class="text-lg font-semibold my-5 flex justify-between items-center mb-5">
      AHP
    </h1>
    <h1 class="pb-2">
      Matriks Perbandingan berpasangan
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Kriteria</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C1</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C2</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C3</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($perbandingan as $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item->ket }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 whitespace-normal">
                {{ number_format((float)$item->n1, 2, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n2, 2, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n3, 2, '.', '') }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Normalisasi Matriks Perbandingan berpasangan
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Kriteria</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C1</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C2</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C3</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Jumlah</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($normalisasi as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item->ket }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 whitespace-normal">
                {{ number_format((float)$item->n1, 3, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n2, 3, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n3, 3, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)($item->n1 + $item->n2 + $item->n3 ), 3, '.', '') }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2 capitalize">
      Nilai eigen vector kriteria
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Kriteria</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Nilai Eigen (Bobot)</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($eigen as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item->ket }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n1, 3, '.', '') }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2 capitalize">
      perkalian Matriks Perbandingan berpasangan dengan nilai eigen vector kriteria
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Hasil</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($perkalian as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item->n1, 3, '.', '') }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
