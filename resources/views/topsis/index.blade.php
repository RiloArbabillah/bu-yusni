@extends('_layouts.default')
@section('content')
<div>
  <div class="py-2">
    <h1 class="text-lg font-semibold my-5 flex justify-between items-center">
      TOPSIS
    </h1>

    <h1 class="pb-2">
      Matriks Keputusan
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Alternatif</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Nama</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C1</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C2</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">C3</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($topsis as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['alternatif'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 whitespace-normal">
                {{ $item['nama'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['c1'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['c2'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['c3'] }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Jarak Euclidien
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Kode</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Ei1</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Ei2</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Ei3</th>
          </tr>
        </thead>
        <tbody>
          <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 whitespace-normal">
              {{ $eucli->judul }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $eucli->ei1 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $eucli->ei2 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $eucli->ei3 }}
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Normalisasi Matriks Keputusan
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">RI</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">R1</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">R2</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">R3</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($topsis as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ 'Ri'.($index+1) }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item['r1'], 4, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item['r2'], 4, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)$item['r3'], 4, '.', '') }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Matriks Solusi Ideal Positif dan Ideal Negatif
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm">y</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm">yi1</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm">yi2</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm">yi3</th>
          </tr>
        </thead>
        <tbody>
          <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              Max (A+)
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_max->r1 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_max->r2 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_max->r3 }}
            </td>
          </tr>
          <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              Min (A-)
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_min->r1 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_min->r2 }}
            </td>
            <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
              {{ $total_min->r3 }}
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Jarak Solusi Ideal Positif dan Negatif Serta Nilai Preferensi
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Alternatif</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">D+</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">D-</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Preferensi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($topsis as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['alternatif'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)
                    $item['d1']
                  , 4, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)
                    $item['d2']
                  , 4, '.', '') }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)
                  $item['d3']
                  , 4, '.', '')
                }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <h1 class="pb-2">
      Peringkat 10 Besar
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow mb-10">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Alternatif</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Nama</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Nilai Preferensi</th>
            <th class="px-6 py-3 border-b tracking-wider leading-4 bg-gray-100 border-gray-200 text-sm uppercase">Peringkat</th>
          </tr>
        </thead>
        <tbody>
          @php
            $peringkat = 1;
          @endphp
          @foreach ($topsis->sortByDesc('d3') as $index => $item)
            <tr class="bg-gray-50 hover:bg-gray-100 text-sm text-center">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['alternatif'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $item['nama'] }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ number_format((float)
                  $item['d3']
                  , 4, '.', '')
                }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $peringkat }}
              </td>
            </tr>
            @php
              $peringkat++
            @endphp
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>
@endsection
