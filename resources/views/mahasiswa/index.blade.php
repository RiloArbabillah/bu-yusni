@extends('_layouts.default')
@section('content')
<div>
  <div class="py-2">
    <h1 class="text-lg font-semibold my-5 flex justify-between items-center">
      Daftar Mahasiswa
      <a href="{{ route('mahasiswa.create') }}"
        class="px-4 py-2 bg-blue-500 rounded-md text-white hover:bg-blue-700 text-base"
      >
        Tambah Mahasiswa
      </a>
    </h1>
    <div class="flex justify-center max-w-7xl rounded inline-block align-middle shadow">
      <table class="min-w-full">
        <thead>
          <tr>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Alternatif</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Nama</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Sikap</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Pengetahuan</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm">Keterampilan</th>
            <th class="px-6 py-3 border-b tracking-wider uppercase leading-4 bg-gray-100 border-gray-200 text-sm"></th>
          </tr>
        </thead>
        <tbody>
          @forelse ($list_mahasiswa as $mahasiswa)
            <tr class="bg-gray-50 hover:bg-gray-100">
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $mahasiswa->alternatif }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 whitespace-normal">
                {{ $mahasiswa->nama }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $mahasiswa->sikap }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $mahasiswa->pengetahuan }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                {{ $mahasiswa->keterampilan }}
              </td>
              <td class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal text-white">
                <div class="flex space-x-3">
                  <a href="{{ route('mahasiswa.edit', $mahasiswa->id) }}"
                    class="px-4 py-2 rounded-md bg-indigo-600 hover:bg-indigo-700"
                  >
                    Edit
                  </a>
                  <form action="{{ route('mahasiswa.destroy', $mahasiswa->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit"
                    class="px-4 py-2 rounded-md bg-red-600 hover:bg-red-700"
                    >
                      Delete
                    </button>
                  </form>
                </div>
              </td>
            </tr>
            @empty
            <tr class="bg-gray-50 hover:bg-gray-100 capitalize">
              <td colspan="7" class="px-6 py-3 leading-5 tracking-wider border-b border-gray-200 text-center whitespace-normal">
                tidak ada data mahasiswa
              </td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
