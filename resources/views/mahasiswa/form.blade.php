@extends('_layouts.default')
@section('content')
  <div>
    <div class="my-8">
      <div class="w-full flex flex-wrap justify-between py-4">
        <h1>{{ isset($mahasiswa)? 'Ubah' : 'Tambah' }} Mahasiswa</h1>
        <a href="{{ route('mahasiswa.index') }}" class="flex space-x-2 text-blue-500 hover:text-blue-800">
          Kembali
        </a>
      </div>
      <form action="{{ isset($mahasiswa)? route('mahasiswa.update', $mahasiswa->id) : route('mahasiswa.store') }}" method="POST"
        class="capitalize bg-white rounded-lg p-5"
      >
      @csrf
      @method(isset($mahasiswa)? 'PUT' : 'POST')
        <div class="flex justify-end items-center space-x-4 px-5 py-5">
          <label for="nama">nama</label>
          <input type="text" value="{{ isset($mahasiswa)? $mahasiswa->nama:'' }}" name="nama" class="rounded-md px-4 py-2 block border border-gray-200 lg:w-3/5">
        </div>
        <div class="flex justify-end items-center space-x-4 px-5 pb-5">
          <label for="sikap">sikap</label>
          <select name="sikap" id="sikap" class="rounded-md px-4 py-2 block border border-gray-200 lg:w-3/5">
            <option value="">Pilih Sikap</option>
            @foreach ($list_sikap as $index => $sikap)
              <option value="{{ $sikap['id'] }}">{{ $sikap['nama'] }}</option>
            @endforeach
          </select>
        </div>
        <div class="flex justify-end items-center space-x-4 px-5 pb-5">
          <label for="pengetahuan">pengetahuan</label>
          <input type="text" value="{{ isset($mahasiswa)? $mahasiswa->pengetahuan:'' }}" name="pengetahuan" class="rounded-md px-4 py-2 block border border-gray-200 lg:w-3/5">
        </div>
        <div class="flex justify-end items-center space-x-4 px-5 pb-5">
          <label for="keterampilan">keterampilan</label>
          <input type="text" value="{{ isset($mahasiswa)? $mahasiswa->keterampilan:'' }}" name="keterampilan" class="rounded-md px-4 py-2 block border border-gray-200 lg:w-3/5">
        </div>
        <div class="border-t border-gray-200 -mx-5 flex justify-end items-center pt-5 px-10">
          <button type="submit" class="px-4 py-2 bg-green-500 hover:bg-green-800 rounded-md text-white">simpan</button>
        </div>
      </form>
    </div>
  </div>
@endsection
