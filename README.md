# Requirement
1. php 7.3+
2. composer
3. mysql
4. git bash (optional)

# How to setup
1. clone / download repository
2. composer install
3. cp .env.example .env
4. change the database name and configuration in .env file
5. php artisan key:generate
6. php artisan migrate:fresh --seed
7. Done

# How to use
1. input all the data in the mahasiswa section
2. check the result in the topsis section.
