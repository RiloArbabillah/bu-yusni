<?php

use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\AhpController;
use App\Http\Controllers\TopsisController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::name('mahasiswa.')->prefix('/mahasiswa')->namespace('Mahasiswa')->group(function () {
    Route::get('/mahasiswa', [MahasiswaController::class, 'index'])->name('index');
    Route::get('/mahasiswa/create', [MahasiswaController::class, 'create'])->name('create');
    Route::post('/mahasiswa/store', [MahasiswaController::class, 'store'])->name('store');
    Route::get('/mahasiswa/edit/{mahasiswa}', [MahasiswaController::class, 'edit'])->name('edit');
    Route::put('/mahasiswa/update/{mahasiswa}', [MahasiswaController::class, 'update'])->name('update');
    Route::delete('/mahasiswa/destroy/{mahasiswa}', [MahasiswaController::class, 'destroy'])->name('destroy');
});

Route::name('ahp.')->prefix('/ahp')->namespace('Ahp')->group(function () {
    Route::get('/ahp', [AhpController::class, 'index'])->name('index');
    Route::get('/ahp/create', [AhpController::class, 'create'])->name('create');
    Route::post('/ahp/store', [AhpController::class, 'store'])->name('store');
    Route::get('/ahp/edit/{ahp}', [AhpController::class, 'edit'])->name('edit');
    Route::put('/ahp/update/{ahp}', [AhpController::class, 'update'])->name('update');
    Route::delete('/ahp/destroy/{ahp}', [AhpController::class, 'destroy'])->name('destroy');
});

Route::name('topsis.')->prefix('/topsis')->namespace('Topsis')->group(function () {
    Route::get('/topsis', [TopsisController::class, 'index'])->name('index');
    Route::get('/topsis/create', [TopsisController::class, 'create'])->name('create');
    Route::post('/topsis/store', [TopsisController::class, 'store'])->name('store');
    Route::get('/topsis/edit/{topsis}', [TopsisController::class, 'edit'])->name('edit');
    Route::put('/topsis/update/{topsis}', [TopsisController::class, 'update'])->name('update');
    Route::delete('/topsis/destroy/{topsis}', [TopsisController::class, 'destroy'])->name('destroy');
});
